using System;
using System.IO;
using McMaster.Extensions.CommandLineUtils;
using Xunit;
using cli_example;

namespace cli_example.test
{
    public class ProgramTest
    {
        #region Generate
        [Fact]
        public void WhenGenerateCommandIsCalledWithoutDestOptionThenCommandIsInvalid()
        {
            CommandLineApplication app = new CommandLineApplication();
            var command = app.Command("test", Program.GenerateAction);
            command.OnValidationError(error => {
                Assert.Equal("The --dest field is required.", error.ErrorMessage);
            });

            int result = command.Execute(new string[] {});

            Assert.Equal(1, result);        
        }

        [Fact]
        public void WhenGenerateCommandIsCalledWithDestOptionWithNotExistingFolderThenCommandIsInvalid()
        {
            CommandLineApplication app = new CommandLineApplication();
            var command = app.Command("test", Program.GenerateAction);
            command.OnValidationError(error => {
                Assert.Equal("The directory ../../../testFiles/invalid does not exists.", error.ErrorMessage);
            });

            int result = command.Execute(new string[] {"--dest", "../../../testFiles/invalid/config.yml"});

            Assert.Equal(1, result);
        }

        [Fact]
        public void WhenGenerateCommandIsCalledWithDestOptionWithExistingFolderThenConfigFileIsGenerated()
        {
            CommandLineApplication app = new CommandLineApplication();
            var command = app.Command("test", Program.GenerateAction);
            var dest = "../../../testFiles/config.yml";
            if (File.Exists(dest)) {
                File.Delete(dest);
            }

            int result = command.Execute(new string[] {"--dest", dest});

            Assert.Equal(0, result);
            Assert.True(File.Exists(dest));
        }

        [Fact]
        public void WhenGenerateCommandIsCalledWithSrcOptionWithNotExistingFileThenCommandIsInvalid()
        {
            CommandLineApplication app = new CommandLineApplication();
            var command = app.Command("test", Program.GenerateAction);
            var srcPath = "../../../testFiles/invalid/config123.yml";
            command.OnValidationError(error => {
                Assert.Equal($"The file '{srcPath}' does not exist.", error.ErrorMessage);
            });

            int result = command.Execute(new string[] {"--src", srcPath});

            Assert.Equal(1, result);
        }
        #endregion

        #region Consume
        [Fact]
        public void WhenConsumeCommandIsCalledWithoutConfigOptionThenCommandIsInvalid()
        {
            CommandLineApplication app = new CommandLineApplication();
            var command = app.Command("test", Program.ConsumeAction);
            command.OnValidationError(error => {
                Assert.Equal("The --config field is required.", error.ErrorMessage);
            });

            int result = command.Execute(new string[] {});

            Assert.Equal(1, result);        
        }

        [Fact]
        public void WhenConsumeCommandIsCalledWithInvalidConfigOptionThenCommandIsInvalid()
        {
            CommandLineApplication app = new CommandLineApplication();
            var command = app.Command("test", Program.ConsumeAction);
            var configPath = "../../../testFiles/invalid/config123.yml";
            command.OnValidationError(error => {
                Assert.Equal($"The file '{configPath}' does not exist.", error.ErrorMessage);
            });

            int result = command.Execute(new string[] {"--config", configPath});

            Assert.Equal(1, result);
        }

        [Fact]
        public void WhenConsumeCommandIsCalledWithValidConfigOptionThenConfigIsComsumed()
        {
            CommandLineApplication app = new CommandLineApplication();
            var command = app.Command("test", Program.ConsumeAction);

            int result = command.Execute(new string[] {"--config", "../../../testFiles/config.yml"});

            Assert.Equal(0, result);
        }

        [Fact]
        public void WhenConsumeCommandIsCalledButContentOfConfigFileIsInvalidThenCommandEndsWithCode1()
        {
            CommandLineApplication app = new CommandLineApplication();
            var command = app.Command("test", Program.ConsumeAction);

            int result = command.Execute(new string[] {"--config", "../../../testFiles/config-invalid.yml"});

            Assert.Equal(1, result);
        }

        [Fact]
        public void WhenConsumeCommandIsCalledWithInvalidSrcOptionThenCommandIsInvalid()
        {
            CommandLineApplication app = new CommandLineApplication();
            var command = app.Command("test", Program.ConsumeAction);
            var srcPath = "../../../testFiles/invalid/config123.yml";
            command.OnValidationError(error => {
                Assert.Equal($"The file '{srcPath}' does not exist.", error.ErrorMessage);
            });

            int result = command.Execute(new string[] {"--config", "../../../testFiles/config.yml", "--src", srcPath});

            Assert.Equal(1, result);
        }

        [Fact]
        public void WhenConsumeCommandIsCalledWithDestOptionWithNotExistingFolderThenCommandIsInvalid()
        {
            CommandLineApplication app = new CommandLineApplication();
            var command = app.Command("test", Program.ConsumeAction);
            command.OnValidationError(error => {
                Assert.Equal("The directory ../../../testFiles/invalid does not exists.", error.ErrorMessage);
            });

            int result = command.Execute(new string[] {"--config", "../../../testFiles/config.yml", "--dest", "../../../testFiles/invalid/config.yml"});

            Assert.Equal(1, result);
        }
        #endregion
    }
}
