﻿using System;
using System.Collections.Generic;
using Microsoft.Extensions.Configuration;
using McMaster.Extensions.CommandLineUtils;
using log4net;
using log4net.Config;
using System.Reflection;
using System.IO;
using YamlDotNet.Serialization;
using McMaster.Extensions.CommandLineUtils.Validation;
using System.ComponentModel.DataAnnotations;

namespace cli_example
{

    public class Program
    {
        private static readonly log4net.ILog logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public static void Main(string[] args)
        {
            var logRepository = LogManager.GetRepository(Assembly.GetEntryAssembly());
            XmlConfigurator.Configure(logRepository, new FileInfo("log4net.config"));
            logger.Info("Logging Configuration Successful. Loaded log4net.config");

            var app = new CommandLineApplication();
            //give people help with --help
            app.HelpOption("-? | -h | --help");

            var calibrate = app.Command("consume", ConsumeAction);
            var corrector = app.Command("generate", GenerateAction);

            var result = app.Execute(args);
            Environment.Exit(result);
        }

        public static Action<CommandLineApplication> GenerateAction = config =>
        {
            config.Description = "Generate a callibration";
            var srcOption = config.Option("--src", "Specify the location for input", CommandOptionType.SingleValue);
            var destOption = config.Option("--dest", "Specify the destination for the configuration file", CommandOptionType.SingleValue);
            srcOption.Accepts().ExistingFile();
            destOption
                .IsRequired()
                .Validators.Add(new FolderMustExist());
            config.OnExecute(() =>
            {
                AppConfig appConfig = getSampleConfig();
                string src = getFullPath(srcOption.Value());
                string dest = getFullPath(destOption.Value());
                writeConfigToYamlFile(appConfig, dest);
                Console.WriteLine("Sample config was generated.");
                return 0;
            });
            config.HelpOption("-? | -h | --help"); //show help on --help
        };

        public static Action<CommandLineApplication> ConsumeAction = config =>
        {
            config.Description = "Consume a config file";
            var configOption = config.Option("--config", "Path to config file", CommandOptionType.SingleValue);
            var srcOption = config.Option("--src", "Specify the location for (other) input.", CommandOptionType.SingleValue);
            var destOption = config.Option("--dest", "Specify the location for output", CommandOptionType.SingleValue);
            configOption
                .IsRequired()
                .Accepts().ExistingFile();
            srcOption.Accepts().ExistingFile();
            destOption.Validators.Add(new FolderMustExist());
            config.OnExecute(() =>
            {
                string conf = getFullPath(configOption.Value());
                string src = getFullPath(srcOption.Value());
                string dest = getFullPath(destOption.Value());

                var appConfig = readConfiguration(conf);
                if (appConfig == null)
                {
                    Console.WriteLine("Configuration file is not valid.");
                    return 1;
                }
                else
                {
                    Console.WriteLine("Configuration loaded succesfully.");
                    writeAppConfigToConsole(appConfig);
                    return 0;
                }
            });
            config.HelpOption("-? | -h | --help"); //show help on --help
        };

        private static void writeConfigToYamlFile(AppConfig conf, string destination)
        {
            var serializer = new Serializer();
            using (StreamWriter outputFile = new StreamWriter(destination))
            {
                serializer.Serialize(outputFile, conf);
            }
        }

        private static AppConfig getSampleConfig()
        {
            var appConfig = new AppConfig();
            appConfig.item1 = "abc";
            appConfig.item2 = true;
            appConfig.item3 = 24;
            appConfig.item4 = 1.24F;
            appConfig.item5 = 23.16;
            var nestConfig = new NestConfig();
            nestConfig.nest1 = "nest";
            nestConfig.nest2 = true;
            nestConfig.nest3 = 67;
            appConfig.nestConfig = nestConfig;
            return appConfig;
        }

        private static AppConfig readConfiguration(string conf)
        {
            IConfigurationRoot configuration = new ConfigurationBuilder()
                                                        .AddYamlFile(conf, optional: false)
                                                        .Build();
            var configurationObject = new AppConfig();

            try
            {
                configuration.Bind(configurationObject);
                return configurationObject;
            }
            catch (InvalidOperationException)
            {
                return null;
            }
        }

        private static string getFullPath(string path)
            => string.IsNullOrEmpty(path) || Path.IsPathRooted(path)
                ? path
                : Path.Combine(Environment.CurrentDirectory, path);

        private static void writeAppConfigToConsole(AppConfig appConfig)
        {           
            Console.WriteLine("Configuration loaded succesfully.");
            Console.WriteLine($"item1: {appConfig.item1}");
            Console.WriteLine($"item2: {appConfig.item2}");
            Console.WriteLine($"item3: {appConfig.item3}");
            Console.WriteLine($"item4: {appConfig.item4}");
            Console.WriteLine($"item5: {appConfig.item5}");
            if (appConfig.nestConfig != null)
            {
                Console.WriteLine($"nest1: {appConfig.nestConfig.nest1}");
                Console.WriteLine($"nest2: {appConfig.nestConfig.nest2}");
                Console.WriteLine($"nest3: {appConfig.nestConfig.nest3}");
            }
        }
    }

    class FolderMustExist : IOptionValidator
    {
        public ValidationResult GetValidationResult(CommandOption option, ValidationContext context)
        {
            if (!option.HasValue())
            {
                return ValidationResult.Success;
            }

            var directory = Path.GetDirectoryName(option.Value());

            return string.IsNullOrEmpty(directory) || Directory.Exists(directory)
                ? ValidationResult.Success
                : new ValidationResult($"The directory {directory} does not exists.");
        }
    }
}
